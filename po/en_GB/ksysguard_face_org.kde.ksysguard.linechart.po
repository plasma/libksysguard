# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# SPDX-FileCopyrightText: 2020, 2021, 2024 Steve Allewell <steve.allewell@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-06 00:41+0000\n"
"PO-Revision-Date: 2024-09-07 12:00+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.05.2\n"

#: contents/ui/Config.qml:42
#, kde-format
msgctxt "@title:group"
msgid "Appearance"
msgstr "Appearance"

#: contents/ui/Config.qml:47
#, kde-format
msgctxt "@option:check"
msgid "Show legend"
msgstr "Show legend"

#: contents/ui/Config.qml:51
#, kde-format
msgctxt "@option:check"
msgid "Stacked charts"
msgstr "Stacked charts"

#: contents/ui/Config.qml:55
#, kde-format
msgctxt "@option:check"
msgid "Smooth lines"
msgstr "Smooth lines"

#: contents/ui/Config.qml:59
#, kde-format
msgctxt "@option:check"
msgid "Show grid lines"
msgstr "Show grid lines"

#: contents/ui/Config.qml:63
#, kde-format
msgctxt "@option:check"
msgid "Show Y axis labels"
msgstr "Show Y axis labels"

#: contents/ui/Config.qml:67
#, kde-format
msgctxt "@label:spinbox"
msgid "Opacity of area below line:"
msgstr "Opacity of area below line:"

#: contents/ui/Config.qml:73
#, kde-format
msgctxt "title:group"
msgid "Data Ranges"
msgstr "Data Ranges"

#: contents/ui/Config.qml:78
#, kde-format
msgctxt "@option:check"
msgid "Automatic Y data range"
msgstr "Automatic Y data range"

#: contents/ui/Config.qml:82
#, kde-format
msgctxt "@label:spinbox"
msgid "From (Y):"
msgstr "From (Y):"

#: contents/ui/Config.qml:89
#, kde-format
msgctxt "@label:spinbox"
msgid "To (Y):"
msgstr "To (Y):"

#: contents/ui/Config.qml:99
#, kde-format
msgctxt "@label:spinbox"
msgid "Show last:"
msgstr "Show last:"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "@item:valuesuffix %1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 second"
msgstr[1] "%1 seconds"

#~ msgid "Fill Opacity:"
#~ msgstr "Fill Opacity:"

#~ msgid "Amount of History to Keep:"
#~ msgstr "Amount of History to Keep:"

#~ msgid "Automatic X Data Range"
#~ msgstr "Automatic X Data Range"

#~ msgid "From (X):"
#~ msgstr "From (X):"

#~ msgid "To (X):"
#~ msgstr "To (X):"
