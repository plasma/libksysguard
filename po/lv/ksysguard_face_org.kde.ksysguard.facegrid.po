# Copyright (C) 2025 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# SPDX-FileCopyrightText: 2025 Toms Trasuns <toms.trasuns@posteo.net>
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-09-06 00:41+0000\n"
"PO-Revision-Date: 2025-01-29 19:04+0200\n"
"Last-Translator: Toms Trasuns <toms.trasuns@posteo.net>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 24.12.1\n"

#: contents/ui/Config.qml:35
#, kde-format
msgctxt "@label:spinbox"
msgid "Number of columns:"
msgstr "Kolonnu skaits:"

#: contents/ui/Config.qml:42
#, kde-format
msgctxt "@label automatic number of columns"
msgid "Automatic"
msgstr "Automātiski"

#: contents/ui/Config.qml:54
#, kde-format
msgctxt "@label:listbox items are sensor face names"
msgid "Display style:"
msgstr "Parādīšanas stils:"
